#!/bin/bash

function get_bin_dir {
    SOURCE=${BASH_SOURCE[0]}
    while [ -h "$SOURCE" ]; do                     # While $SOURCE is still a link (-h)
      DIR=$( cd -P $( dirname "$SOURCE") && pwd )
      SOURCE=$(readlink "$SOURCE")                 # resolve it as absolute ...  
      [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # .. or relative link
    done
    DIR=$( cd -P $( dirname "$SOURCE" ) && pwd )
    echo $DIR
}

BIN_DIR=$(get_bin_dir)

if [ -e "${BIN_DIR}/../installdata/sapjvm_8_cons/bin/java" ]; then
    JAVA_HOME=${BIN_DIR}/../installdata/sapjvm_8_cons
elif [ -e "${BIN_DIR}/../installdata/runtimes/sapjvm_8_rel/bin/java" ]; then
    JAVA_HOME=${BIN_DIR}/../installdata/runtimes/sapjvm_8_rel
elif [ -e "${BIN_DIR}/../sapjvm_8/bin/java" ]; then
    JAVA_HOME=${BIN_DIR}/../sapjvm_8
elif [ -e "${BIN_DIR}/../sapjvm_8_jre/bin/java" ]; then
    JAVA_HOME=${BIN_DIR}/../sapjvm_8_jre
fi

if test "$JAVA_HOME"x = x
then
    echo "JAVA_HOME must be set"
    exit 1
fi

JAVA="$JAVA_HOME/bin/java"
JARSDIR=$BIN_DIR/../jars
cd "$JARSDIR"
JARSDIR=`pwd`
cd - > /dev/null

CLASSPATH="$JARSDIR"/xs2rt_1.0_client_api.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/xs2rt_1.0_client_impl.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/xs2rt_1.0_core.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/xs2rt_1.0_tracing_api.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/xs2rt_1.0_utils.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/aopalliance-repackaged-2.4.0-b10.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/hk2-api-2.4.0-b10.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/hk2-locator-2.4.0-b10.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/hk2-utils-2.4.0-b10.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jackson-annotations-2.5.0.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jackson-core-2.5.0.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jackson-databind-2.5.0.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jackson-jaxrs-base-2.5.0.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jackson-jaxrs-json-provider-2.5.0.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jackson-module-jaxb-annotations-2.5.0.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/javax.annotation-api-1.2.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/javax.inject-2.4.0-b10.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/javax.ws.rs-api-2.0.1.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/javassist-3.19.0-GA.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jersey-client-2.24.1.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jersey-common-2.24.1.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jersey-guava-2.24.1.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jersey-entity-filtering-2.24.1.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jersey-media-json-jackson-2.24.1.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/org.eclipse.jgit-4.0.1.201506240215-r.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/osgi-resource-locator-1.0.1.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/snakeyaml-1.15.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/ini4j-0.5.4.jar
# deps for bean validation
CLASSPATH="$CLASSPATH":"$JARSDIR"/hibernate-validator-5.1.3.Final.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/validation-api-1.1.0.Final.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/javax.el-2.2.4.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/javax.el-api-2.2.4.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jboss-logging-3.1.3.GA.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/classmate-1.0.0.jar
# deps for websocket debugging communication
CLASSPATH="$CLASSPATH":"$JARSDIR"/websocket-api-9.3.9.v20160517.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/websocket-client-9.3.9.v20160517.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/websocket-common-9.3.9.v20160517.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jetty-util-9.3.9.v20160517.jar
CLASSPATH="$CLASSPATH":"$JARSDIR"/jetty-io-9.3.9.v20160517.jar

KERNEL_NAME=`uname -s`
PLATFORM_NAME=`uname -m`

if [[ $KERNEL_NAME == "AIX" || $PLATFORM_NAME == "ppc64" || $PLATFORM_NAME == "ppc64le" ]]; then
    STACK_SIZE=512
else
    STACK_SIZE=256
fi

JVM_OPTS="-Xmx128m -Xms64m -XX:MetaspaceSize=64m -XX:ThreadStackSize=${STACK_SIZE} -XX:ReservedCodeCacheSize=48M"
JVM_OPTS="${JVM_OPTS} -XX:MaxGCThreads=2 -Dcom.sap.jvm.vmtag=xs"

exec "$JAVA" ${JVM_OPTS} -cp "$CLASSPATH" -Dhttp.keepAlive=false com.sap.xs2rt.client.impl.Main "$@"
